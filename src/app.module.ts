import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './cats/cat.controller';
import { CatsModule } from './cats/cats.module';
import { CatsService } from './cats/cats.service';
import { logger } from './middleware/logger.middleware';

@Module({
  imports: [CatsModule]

})
export class AppModule {
  configure(consumer: MiddlewareConsumer){
    consumer
      .apply(logger)
      .forRoutes({path: 'cats', method: RequestMethod.GET})
  }
}
