import { HttpException, HttpStatus } from "@nestjs/common";

export class ForbiddenException extends HttpException {
    constructor() {
        super('Cat cannot be forbidden', HttpStatus.FORBIDDEN);
    }
}